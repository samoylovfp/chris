//! # Neural network for two-legged walking.
//! 
//! ## Inputs
//! - body height relative to the terrain below (1 * f32)
//! - body angles (3 * f32) - should a quaternion be used?
//! - body velocity vector (3 * f32)
//! - 2 *
//!   - hip joint angles (3 * f32)
//!   - shin joint angle (1 * f32)
//!   - foot joint angle (1 * f32)
//! 
//! ## Outputs
//! - 2 *
//!   - hip joint angles (3 * f32)
//!   - shin joint angle (1 * f32)
//!   - foot joint angle (1 * f32)
//! 

type NN = (

);