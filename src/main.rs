mod chris_nn;

use std::collections::VecDeque;
use std::str::FromStr;

use bevy::{prelude::*, render::mesh::Indices};
use bevy_egui::egui;
use bevy_egui::{egui::Slider, EguiContext, EguiPlugin};
use bevy_rapier3d::prelude::*;
use bevy_rapier3d::rapier::prelude::JointAxis;
use itertools::Itertools;

fn main() {
    App::new()
        // default bevy
        .add_plugins(DefaultPlugins)
        // camera
        .add_plugin(bevy_flycam::PlayerPlugin)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(EguiPlugin)
        .add_startup_system(setup)
        .add_system(rotate_joints)
        .add_system(ui)
        .run();
}

#[derive(Component)]
struct JointRotation {
    angle: RotationAngle,
    joint_type: JointType,
}

enum RotationAngle {
    Linear(f32),
    Spherical(f32, f32),
}

#[derive(PartialEq, Eq, strum::EnumString, strum::AsRefStr)]
enum JointType {
    LFoot,
    RFoot,

    LShin,
    RShin,

    LThigh,
    RThigh,
}

/// set up a simple 3D scene
fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // plane
    commands
        .spawn(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane { size: 500.0 })),
            material: materials.add(Color::rgb(0.6, 0.4, 0.3).into()),
            ..default()
        })
        .insert(RigidBody::Fixed)
        .insert(Collider::cuboid(250.0, 0.1, 250.0));
    // light
    commands.spawn(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..default()
    });

    add_robot(commands, meshes, materials)
}

fn add_robot(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let max_force = 400.0;
    // body
    let material = materials.add(Color::rgb(0.3, 0.5, 0.3).into());

    let model = wavefront::Obj::from_file("rob.obj").unwrap();
    debug!("Creating body");

    let body = entity_from_obj(
        model.object("Body").unwrap(),
        &mut commands,
        &mut meshes,
        material.clone(),
    );
    for side in ["L", "R"] {
        let mut parent = body;
        for part_name in ["Thigh", "Shin", "Foot"] {
            let full_name = format!("{side}{part_name}");
            debug!("Creating {full_name}");
            let part = entity_from_obj(
                model.object(&full_name).unwrap(),
                &mut commands,
                &mut meshes,
                material.clone(),
            );
            let joint_name = format!("{side}{part_name}Joint");
            let joint_pos = get_first_vertex_pos(
                &model
                    .object(&joint_name)
                    .unwrap_or_else(|| panic!("Joint {joint_name} not found")),
            );
            if part_name == "Thigh" {
                let joint = SphericalJointBuilder::new()
                    .local_anchor1(joint_pos.into())
                    .local_anchor2(joint_pos.into())
                    .motor_max_force(JointAxis::AngX, max_force)
                    .motor_max_force(JointAxis::AngZ, max_force);
                commands
                    .entity(part)
                    .insert(ImpulseJoint::new(parent, joint))
                    .insert(JointRotation {
                        angle: RotationAngle::Spherical(0.0, 0.0),
                        joint_type: JointType::from_str(&full_name).unwrap(),
                    });
            } else {
                let joint = RevoluteJointBuilder::new(Vec3 {
                    x: 1.0,
                    y: 0.0,
                    z: 0.0,
                })
                .local_anchor1(joint_pos.into())
                .local_anchor2(joint_pos.into())
                .motor_max_force(max_force);
                commands
                    .entity(part)
                    .insert(ImpulseJoint::new(parent, joint))
                    .insert(JointRotation {
                        angle: RotationAngle::Linear(0.0),
                        joint_type: JointType::from_str(&full_name).unwrap(),
                    });
            };
            parent = part;
        }
    }
}

fn get_first_vertex_pos(object: &wavefront::Object) -> [f32; 3] {
    object
        .polygons()
        .next()
        .unwrap()
        .vertices()
        .next()
        .unwrap()
        .position()
}

fn entity_from_obj(
    model: wavefront::Object,
    commands: &mut Commands,
    meshes: &mut ResMut<Assets<Mesh>>,
    material: Handle<StandardMaterial>,
) -> Entity {
    let mesh = meshes.add(mesh_from_obj(model));
    commands
        .spawn(PbrBundle {
            mesh,
            material,
            ..default()
        })
        .insert(RigidBody::Dynamic)
        .insert(bevy_collider_from_obj(model))
        .id()
}

fn mesh_from_obj(model: wavefront::Object) -> Mesh {
    let mut result = Mesh::new(bevy::render::render_resource::PrimitiveTopology::TriangleList);
    let mut vertices = vec![];
    let mut indices = vec![];
    let mut normals = vec![];

    for polygon in model.polygons() {
        let mut verts = polygon.vertices();
        let v1 = verts.next().expect("At least one vert in a poly");

        for (v2, v3) in verts.tuple_windows() {
            for v in [v1, v2, v3] {
                vertices.push(v.position());
                normals.push(v.normal().unwrap());
            }
            let i = vertices.len() as u16;
            indices.extend([i - 3, i - 2, i - 1]);
        }
    }
    assert!(vertices.len() >= 3);

    result.insert_attribute(Mesh::ATTRIBUTE_POSITION, vertices);
    result.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    result.set_indices(Some(Indices::U16(indices)));

    result
}

fn bevy_collider_from_obj(model: wavefront::Object) -> Collider {
    let mut vertices = vec![];
    let mut indices = vec![];

    for polygon in model.polygons() {
        let mut verts = polygon.vertices();
        let v1 = verts.next().expect("At least one vert in a poly");

        for (v2, v3) in verts.tuple_windows() {
            for v in [v1, v2, v3] {
                vertices.push(v.position().into());
            }
            let i = vertices.len() as u32;
            indices.push([i - 3, i - 2, i - 1]);
        }
    }

    Collider::trimesh(vertices, indices)
}

fn rotate_joints(mut joints: Query<(&mut ImpulseJoint, &JointRotation)>) {
    let stiffness = 50000.0;
    let damping = 0.01;
    joints.for_each_mut(|(mut j, rot)| match rot.angle {
        RotationAngle::Linear(rot) => {
            j.data.as_revolute_mut().unwrap().set_motor_position(
                rot.to_radians(),
                stiffness,
                damping,
            );
        }
        RotationAngle::Spherical(rot_x, rot_y) => {
            j.data
                .as_spherical_mut()
                .unwrap()
                .set_motor_position(JointAxis::AngX, rot_x.to_radians(), stiffness, damping)
                .set_motor_position(JointAxis::AngZ, rot_y.to_radians(), stiffness, damping);
        }
    });
}

fn ui(
    mut egui_context: ResMut<EguiContext>,
    mut joint_rotations: Query<&mut JointRotation>,
) {
    egui::Window::new("Hello").show(egui_context.ctx_mut(), |ui| {
        ui.label("world");
        for mut j in joint_rotations.iter_mut() {
            ui.horizontal(|ui| {
                ui.label(j.joint_type.as_ref());
                match &mut j.angle {
                    RotationAngle::Linear(rot) => {
                        ui.add(Slider::new(rot, -90.0..=90.0));
                    }
                    RotationAngle::Spherical(rot_x, rot_y) => {
                        ui.add(Slider::new(rot_x, -90.0..=90.0));
                        ui.add(Slider::new(rot_y, -90.0..=90.0));
                    }
                }
            });
        }
    });
}
